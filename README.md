# staff-tracker-specflow

## Instalar extensión SpecFlow
Abrir IDE (Visual Studio o JetBrains Rider) e instalar extensión de SpecFlow siguiendo https://docs.specflow.org/projects/getting-started/en/latest/index.html


## Crear estructura de solución y dependencias
Crear solución y, opcionalmente, `.gitignore`

```
dotnet new sln
dotnet new gitignore
```

Crear librería de clases
```
dotnet new classlib --name StaffTrack --output src/StaffTrack
```

Añadir proyecto de librería de clases a la solución
```
dotnet sln add src/StaffTrack
```

Crear proyecto de tests funcionales
```
dotnet new xunit --name StaffTrack.FunctionalTests --output test/StaffTrack.FunctionalTests
```

Añadir dependencia del proyecto de tests funcionales al proyecto de librería de clases
```
dotnet add test/StaffTrack.FunctionalTests reference src/StaffTrack
```

Instalar paquetes SpecFlow en el proyecto de tests funcionales
```
dotnet add test/StaffTrack.FunctionalTests package SpecFlow.xUnit
dotnet add test/StaffTrack.FunctionalTests package SpecFlow.Plus.LivingDocPlugin
```

Instalar paquete de Fluent Assertions en el proyecto de tests funcionales
```
dotnet add test/StaffTrack.FunctionalTests package FluentAssertions
```

Añadir proyecto de tests funcionales a la solución
```
dotnet sln add test/StaffTrack.FunctionalTests
```

Abrir solución y asegurarse de que los tests se ejecutan correctamente (e.g: CTRL + U + L)

## Ejercicio
Crea una clase `Employee` con `int Id` y `string Name`.

Crea una clase `Tracker` con métodos
```
void CheckIn(Employee employee);
void CheckOut(Employee employee);
IEnumerable<Employee> GetActiveEmployees();
```

Implementa estos métodos para satisfacer la siguiente funcionalidad.

Crea un `GetEmployees.feature`

```
Feature: Get Active Employees
	Reads all the employees that are currently working

Scenario: Should take into account checked-in and checked-out employees when getting all active employees
	Given an employee with id 1 and name Pepe checks in
	And an employee with id 2 and name María checks in
	And an employee with id 3 and name Lucía checks in
	And an employee with id 4 and name José checks out
	And an employee with id 2 and name María checks in
	And an employee with id 5 and name Pablo checks in
	When getting all active employees
	Then there should be 4 employees
	And there should not be any active employee with name José
```

### Livingdoc
Specflow viene con una herramienta llamada livingdoc. En el proyecto de tests hemos instalado un paquete `SpecFlow.Plus.LivingDocPlugin` que, hasta ahora, no se ha utilizado. Este paquete va a permitir a la herramienta livingdoc, conocer todos las features y escenarios existentes, y el resultado de la ejecución de tests, y generar un reporte en html.

Para ello, instala la herramienta
```
dotnet tool install --global SpecFlow.Plus.LivingDoc.CLI
```

Y ejecuta los tests con esta herramienta indicando la assembly adecuada
```
livingdoc test-assembly --title StaffTrack -o resultado.html test/StaffTrack.FunctionalTests/bin/Debug/net6.0/StaffTrack.FunctionalTests.dll -t test/StaffTrack.FunctionalTests/bin/Debug/net6.0/TestExecution.json
```