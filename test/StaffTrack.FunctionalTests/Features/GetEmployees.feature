Feature: Get Employees
	Reads all the employees that are currently working

Background: 
	Given an employee with id 1 and name Pepe checks in
	And an employee with id 2 and name María checks in
	
@check-in @check-out @getEmployees
Scenario: Should take into account checked-in employees when getting all active employees
	When getting all active employees
	Then there should be 2 active employees
	And there should be an employee with id 1 and with name Pepe
	And there should be an employee with id 2 and with name María
	
@check-in @check-out @getEmployees
Scenario: Should take into account checked-in and checked-out employees when getting all active employees
	Given an employee with id 3 and name Lucía checks in
	And an employee with id 3 and name Lucía checks out
	When getting all active employees
	Then there should be 2 active employees
	And there should be an employee with id 1 and with name Pepe
	And there should be an employee with id 2 and with name María
	#And there should not be an employee with id 3 and with name Lucía