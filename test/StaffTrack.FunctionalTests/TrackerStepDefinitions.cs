using FluentAssertions;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;

namespace StaffTrack.FunctionalTests;

[Binding]
public class TrackerStepDefinitions
{
    private readonly Tracker _tracker = new Tracker();
    private IEnumerable<Employee> _resultActiveEmployees = new List<Employee>();

    [Given(@"an employee with id (.*) and name (.*) checks in")]
    public void GivenAnEmployeeWithIdAndNameChecksIn(int id, string name)
    {
        var employee = new Employee(id, name);
        _tracker.CheckIn(employee);
    }
    
    [Given(@"an employee with id (.*) and name (.*) checks out")]
    public void GivenAnEmployeeWithIdAndNameChecksOut(int id, string name)
    {
        var employee = new Employee(id, name);
        _tracker.CheckOut(employee);
    }

    [When(@"getting all active employees")]
    public void WhenGettingAllActiveEmployees()
    {
        _resultActiveEmployees = _tracker.GetActiveEmployees();
    }

    [Then(@"there should be (.*) active employees")]
    public void ThenThereShouldBeActiveEmployees(int count)
    {
        //_resultActiveEmployees.Count().Should().Be(count);
        _resultActiveEmployees.Should().HaveCount(count);
    }

    [Then(@"there should be an employee with id (.*) and with name (.*)")]
    public void ThenThereShouldBeAnEmployeeWithIdAndWithName(int id, string name)
    {
        _resultActiveEmployees.Should().Contain(x => x.Id == id && x.Name == name);
        //_resultActiveEmployees.Any(x => x.Id == id && x.Name == name).Should().BeTrue();
    }
}