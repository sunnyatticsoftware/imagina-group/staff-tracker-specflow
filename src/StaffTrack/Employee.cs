namespace StaffTrack;

public record Employee (int Id, string Name);