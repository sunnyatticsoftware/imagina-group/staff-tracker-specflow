namespace StaffTrack;

public class Tracker
{
    private readonly IList<Employee> _employees = new List<Employee>();

    public void CheckIn(Employee employee)
    {
        _employees.Add(employee);
    }

    public void CheckOut(Employee employee)
    {
        var result = _employees.Remove(employee);
    }

    public IEnumerable<Employee> GetActiveEmployees()
    {
        return _employees;
    }
}